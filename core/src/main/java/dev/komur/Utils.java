package dev.komur;

import static dev.komur.StringUtils.isPositiveNumber;

public class Utils {

    public static boolean isAllPositiveNumbers(String... str){
        if (str.length==0) {
            return false;
        }

        for (String s : str) {
            if (!isPositiveNumber(s)) {
                return false;
            }
        }
        return true;
    }
}
